import java.io.*;
import java.util.*;


/**
 * Student Managment System
 */
public class StudentManagement {

    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<Student>();

        try {
            loadStudentsFromFile(studentList, "student.txt");
        } catch (IOException e) {
            System.err.println("Error loading students from file: " + e.getMessage());
        }

        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            System.out.println("Student Management System Menu:");
            System.out.println("1. Print Students");
            System.out.println("2. Add Student");
            System.out.println("3. Delete Student");
            System.out.println("4. Update Student");
            System.out.println("5. Search Student by Name");
            System.out.println("6. Search Student by ID");
            System.out.println("7. Load Students from File");
            System.out.println("8. Save Students to File");
            System.out.println("9. Exit");
            System.out.print("Enter your choice: ");

            choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            switch (choice) {
                case 1:
                    printStudents(studentList);
                    break;
                case 2:
                    addStudent(studentList, scanner);
                    break;
                case 3:
                    deleteStudent(studentList, scanner);
                    break;
                case 4:
                    updateStudent(studentList, scanner);
                    break;
                case 5:
                    searchStudentByName(studentList, scanner);
                    break;
                case 6:
                    searchStudentById(studentList, scanner);
                    break;
                case 7:
                    try {
                        loadStudentsFromFile(studentList, "student.txt");
                    } catch (IOException e) {
                        System.err.println("Error loading students from file: " + e.getMessage());
                    }
                    break;
                case 8:
                    try {
                        saveStudentsToFile(studentList, "student.txt");
                    } catch (IOException e) {
                        System.err.println("Error saving students to file: " + e.getMessage());
                    }
                    break;
                case 9:
                    System.out.println("Exiting the program.");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 9);

        scanner.close();
    }

    /**
     * Print the details of all students in the list.
     *
     * @param studentList The list of students to print.
     */
    private static void printStudents(List<Student> studentList) {
        for (Student student : studentList) {
            System.out.println("Name: " + student.name);
            System.out.println("Age: " + student.age);
            System.out.println("ID: " + student.id);
            System.out.println("Address: " + student.address);
            System.out.println("Phone Number: " + student.phoneNumber);
            System.out.println("Email: " + student.email);
            System.out.println();
        }
    }

    /**
     * Add a new student to the list.
     *
     * @param studentList The list of students to add to.
     * @param scanner     The scanner for user input.
     */
    private static void addStudent(List<Student> studentList, Scanner scanner) {
        Student newStudent = new Student();
        System.out.print("Enter student name: ");
        newStudent.name = scanner.nextLine().trim();

        if (newStudent.name.isEmpty()) {
            System.out.println("Name cannot be empty.");
            return;
        }

        System.out.print("Enter student age: ");
        String ageInput = scanner.nextLine();

        try {
            newStudent.age = Integer.parseInt(ageInput);
        } catch (NumberFormatException e) {
            System.out.println("Invalid age format. Age should be a number.");
            return;
        }

        System.out.print("Enter student ID: ");
        String idInput = scanner.nextLine();

        try {
            newStudent.id = Integer.parseInt(idInput);
        } catch (NumberFormatException e) {
            System.out.println("Invalid ID format. ID should be a number.");
            return;
        }

        System.out.print("Enter student address: ");
        newStudent.address = scanner.nextLine().trim();

        System.out.print("Enter student phone number: ");
        String phoneNumberInput = scanner.nextLine();

        if (!phoneNumberInput.matches("^\\d+$")) {
            System.out.println("Invalid phone number format. Phone number should only contain numbers.");
            return;
        }

        newStudent.phoneNumber = phoneNumberInput;

        System.out.print("Enter student email: ");
        newStudent.email = scanner.nextLine().trim();

        // Check for duplicates based on ID
        if (isIdUnique(studentList, newStudent.id)) {
            studentList.add(newStudent);
            System.out.println("Student added: " + newStudent.name);
        } else {
            System.out.println("Student with ID " + newStudent.id + " already exists.");
        }
    }

    /**
     * Delete a student from the list by name.
     *
     * @param studentList The list of students to delete from.
     * @param scanner     The scanner for user input.
     */
    private static void deleteStudent(List<Student> studentList, Scanner scanner) {
        System.out.print("Enter student name to delete: ");
        String nameToDelete = scanner.nextLine();

        Iterator<Student> iterator = studentList.iterator();
        while (iterator.hasNext()) {
            Student student = iterator.next();
            if (student.name.equals(nameToDelete)) {
                iterator.remove();
                System.out.println(nameToDelete + " has been deleted.");
            }
        }
    }

    /**
     * Update student information by name.
     *
     * @param studentList The list of students to update.
     * @param scanner     The scanner for user input.
     */
    private static void updateStudent(List<Student> studentList, Scanner scanner) {
        System.out.print("Enter student name to update: ");
        String nameToUpdate = scanner.nextLine();

        for (Student student : studentList) {
            if (student.name.equals(nameToUpdate)) {
                System.out.print("Enter new name: ");
                student.name = scanner.nextLine().trim();

                if (student.name.isEmpty()) {
                    System.out.println("Name cannot be empty.");
                    return;
                }

                System.out.print("Enter new age: ");
                String ageInput = scanner.nextLine();

                try {
                    student.age = Integer.parseInt(ageInput);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid age format. Age should be a number.");
                    return;
                }

                System.out.print("Enter new ID: ");
                String idInput = scanner.nextLine();

                try {
                    student.id = Integer.parseInt(idInput);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid ID format. ID should be a number.");
                    return;
                }

                System.out.print("Enter new address: ");
                student.address = scanner.nextLine().trim();

                System.out.print("Enter new phone number: ");
                String phoneNumberInput = scanner.nextLine();

                if (!phoneNumberInput.matches("^\\d+$")) {
                    System.out.println("Invalid phone number format. Phone number should only contain numbers.");
                    return;
                }

                student.phoneNumber = phoneNumberInput;

                System.out.print("Enter new email: ");
                student.email = scanner.nextLine().trim();

                System.out.println(nameToUpdate + " has been updated.");
                return;
            }
        }

        System.out.println("Student not found: " + nameToUpdate);
    }

    /**
     * 
     * @param studentList The list of students to search in by name
     * @param scanner The scanner for user input.
     */
    private static void searchStudentByName(List<Student> studentList, Scanner scanner) {
        System.out.print("Enter student name to search: ");
        String nameToSearch = scanner.nextLine();

        for (Student student : studentList) {
            if (student.name.equals(nameToSearch)) {
                System.out.println("Student found:");
                System.out.println("Name: " + student.name);
                System.out.println("Age: " + student.age);
                System.out.println("ID: " + student.id);
                System.out.println("Address: " + student.address);
                System.out.println("Phone Number: " + student.phoneNumber);
                System.out.println("Email: " + student.email);
                return;
            }
        }

        System.out.println("Student not found: " + nameToSearch);
    }

    /**
     * 
     * @param studentList The list of student to search in by ID
     * @param scanner The scanner for user input
     */

    private static void searchStudentById(List<Student> studentList, Scanner scanner) {
        System.out.print("Enter student ID to search: ");
        int idToSearch = scanner.nextInt();
        scanner.nextLine(); // Consume the newline character

        for (Student student : studentList) {
            if (student.id == idToSearch) {
                System.out.println("Student found:");
                System.out.println("Name: " + student.name);
                System.out.println("Age: " + student.age);
                System.out.println("ID: " + student.id);
                System.out.println("Address: " + student.address);
                System.out.println("Phone Number: " + student.phoneNumber);
                System.out.println("Email: " + student.email);
                return;
            }
        }

        System.out.println("Student with ID " + idToSearch + " not found.");
    }

    /**
     * 
     * @param studentList The list of students to check
     * @param idToCheck The ID to check for uniqueness 
     * @return True if the ID is unique, false otherwise
     */

    private static boolean isIdUnique(List<Student> studentList, int idToCheck) {
        for (Student student : studentList) {
            if (student.id == idToCheck) {
                return false; // ID already exists
            }
        }
        return true; // ID is unique
    }

    /**
     * 
     * @param studentList The list of student to load
     * @param fileName The name of file to read data from
     * @throws IOException If an I/O error occurs
     */

    private static void loadStudentsFromFile(List<Student> studentList, String fileName) throws IOException {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split(" ");
                if (parts.length >= 6) {
                    Student student = new Student();
                    student.name = parts[0];
                    student.age = Integer.parseInt(parts[1]);
                    student.id = Integer.parseInt(parts[2]);
                    student.address = parts[3];
                    student.phoneNumber = parts[4];
                    student.email = parts[5];
                    studentList.add(student);
                }
            }

            System.out.println("Students loaded from file: " + fileName);
        } catch (FileNotFoundException e) {
            System.err.println("File not found: " + fileName);
            throw e;
        } catch (NumberFormatException e) {
            System.err.println("Invalid data format in the file: " + fileName);
            throw e;
        }
    }


    /**
     * 
     * @param studentList The list of students to save
     * @param fileName The name of file to write data to
     * @throws IOException If an I/O error occurs
     */
    private static void saveStudentsToFile(List<Student> studentList, String fileName) throws IOException {
        try (FileWriter fileWriter = new FileWriter(fileName);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            for (Student student : studentList) {
                String line = student.name + " " + student.age + " " + student.id + " " +
                        student.address + " " + student.phoneNumber + " " + student.email;
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }

            System.out.println("Students saved to file: " + fileName);
        } catch (IOException e) {
            System.err.println("Error saving data to file: " + fileName);
            throw e;
        }
    }
}