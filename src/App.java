public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        System.out.println("Hello, World! Again");

        int a = 10;
        int b = 20;
        int c = a + b;
        System.out.println("Sum of hte two numbers is " + c);

        // comparison statements
        if (c > 10) {
            System.out.println("The value is greater than 10");
        } else {
            System.out.println("The value is not greater than 10");

        }

        // for (int i = 1; i <= 10; i++) {
        // System.out.println(i);
        // }
        int previous = 0;
        int next = 1;
        int last = 0;
        System.out.println(previous + "\n" + next);
        for (int i = 0; i <= 10; i++) {
            last = previous + next;
            System.out.println(last);
            previous = next;
            next = last;
        }
        System.out.println();
        int[] array1 = new int[10];

        LoadArray(array1);
        PrintArray(array1);
        System.out.println();
        Replace(array1, 6, 20);
        DeleteVal(array1, 4);
        System.out.println();
        PrintArray(array1);

        System.out.println();

        String[] stringArray = new String[10];
        LoadString(stringArray);
        PrintString(stringArray);

    }

    private static void PrintArray(int[] arr) {
        for (int i = 0; i < 10; i++) {
            System.out.println(arr[i]);
        }
    }

    private static void PrintString(String[] arr) {
        for (int i = 0; i < 10; i++) {
            System.out.println(arr[i]);
        }
    }

    private static void LoadArray(int[] arr) {
        for (int i = 0; i < 10; i++) {
            arr[i] = i;
        }
    }

    private static void LoadString(String[] arr) {
        for (int i = 0; i < 10; i++) {
            arr[i] = "hello";
        }
    }

    private static void Replace(int[] arr, int index, int val) {
        arr[index] = val;
    }

    private static void ReplaceString(String[] arr, int index, String val) {
        arr[index] = val;
    }

    private static void DeleteVal(int[] arr, int index) {
        arr[index] = 0;
    }

    private static void DeleteString(String[] arr, int index) {
        arr[index] = "World";
    }

}
