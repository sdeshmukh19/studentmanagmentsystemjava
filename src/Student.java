public class Student {
    public String name;
    public int age;
    public int id; // Student ID
    public String address;
    public String phoneNumber;
    public String email;
}
